def read_students_from_file(filename: str):
    students_lst = []
    with open(filename) as file:
        for line in file:
            parts = line.split()
            name = parts[0]
            marks = list(map(int, parts[1:]))
            students_lst.append(Student(name, marks))
    return students_lst

class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks

    def __gt__(self, other):
        if all(x > y for x, y in zip(self.marks, other.marks)):
            return True
        return False

    def __repr__(self):
        return f'{self.name} -> {self.marks}'


def compare_students(students_lst):
    comparable = []   # name of students having comparable marks
    uncomparable = []   # name of students whose marks aren't comparable
    for marks in range(len(students_lst)):
        for i in range(marks + 1, len(students_lst)):
            if students_lst[marks] > students_lst[i]:
                comparable.append(f"{students_lst[marks].name} > {students_lst[i].name}")
            elif students_lst[marks] < students_lst[i]:
                comparable.append(f"{students_lst[i].name} > {students_lst[marks].name}")
            else:
                uncomparable.append(f"{students_lst[marks].name} and {students_lst[i].name}")
    return comparable, uncomparable



print(compare_students(read_students_from_file("students.txt")))